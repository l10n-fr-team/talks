Title: Sprint de l'équipe de localisation en langue française à Marseille le 24 mai 2019
---------------------------------

Après une discussion de l'équipe d'organisation de la
[MiniDefConf de Marseille](https://wiki.debian.org/DebianEvents/fr/2019/Marseille),
un des membres de l'équipe de localisation (la l10n-french team),
Alban, a proposé sur la [liste](https://lists.debian.org/debian-l10n-french/2018/12/msg00028.html)
la réalisation d'une présentation de l'équipe de traduction, de ses
objectifs et de son fonctionnement.

Aussitôt, Thomas et Jean-Pierre ont suggéré sur la liste l'organisation, en
marge de la MiniDebConf, d'un [sprint](https://wiki.debian.org/Sprints) de
l'équipe (le premier – même s'il avait été proposé il y a plusieurs années
d'organiser une séance aux Antilles… hélas sans succès). Après un
[sondage](https://framadate.org/debian-l10n-minidebconf), la date en a été
fixée et un espace nous a été proposé par l'équipe d'organisation
de la MiniDebConf - que nous remercions pour la chaleur de l'accueil.

Le sprint s'est tenu le jour précédant la MiniDefConf de 9h30
à 18h00, et l'équipe a continué à se rencontrer pour poursuivre
différentes tâches liées aux traductions.

Cinq membres de l'équipe (Alban, Jean-Philippe, Jean-Pierre, Quentin et
Thomas), ont participé aux travaux dans une ambiance des plus amicales. Une
partie du temps a été consacrée à la finition de la
[présentation](https://wiki.debian.org/DebianEvents/fr/2019/Marseille?action=AttachFile&do=view&target=La+localisation+de+Debian+en+langue+fran%C3%A7aise.pdf)
et a permis de discuter plusieurs points sur les objectifs et priorités
du travail de traduction :

Définitions des priorités de traduction :
- traduction des paquets propres à la distribution Debian (dpkg, apt,
aptitude…) et notamment de leurs écrans d'installation (debconf)

- traduction des pages du site Debian

- traduction de la description des paquets, ce qui a donné l'occasion
d'initier certains membres de l'équipe à l'utilisation de l'interface
web de traduction, le [DDTSS](https://ddtp2.debian.net/ddtss/index.cgi/fr),
parfois capricieux et sans nul doute perfectible et dont la prise en
charge dans debian.org, souvent évoquée, est encore attendue. Cet outil
n'a pas permis à ce jour de régler la question d'une traduction du paquet
_texlive-latex-extra_, impossible à traiter pour le moment. L'équipe a parlé de
se rapprocher de l'équipe italienne pour y trouver des sources d'inspiration
méthodologique.

La rencontre a aussi été l'occasion d’approfondir la discussion,
déjà abordée sur la liste, sur la traduction de paquets créés et
maintenus hors de la distribution (manpages par exemple) et les relations
avec les développeurs amont. Les droits de push sur le paquet maintenu par
l'équipe ont été mis à jour.

Dans le prolongement du sprint, un travail a été entrepris par Thomas
et Jean-Pierre pour améliorer certains outils propres à la distribution
destinés à faciliter la tâche de traduction, et notamment pour les
adapter à l’utilisation de Git et de [Salsa](https://salsa.debian.org/) :
scripts pour la traduction des DSA ou pour la récupération des annonces et
nouvelles et leur mise en forme pour envoi aux listes de diffusion. Après
test, ces scripts pourront être proposés aux équipes de traduction dans
d'autres langues.

La présentation de l’équipe, par Alban, Jean-Philippe et Thomas, a été
plutôt bien reçue par les participants à la MiniDebConf, et a permis aux
membres de l’équipe de rencontrer plusieurs développeurs Debian dont
certains se sont investis dans les efforts de traduction, durant leur
participation à la communauté Debian.
La vidéo de la présentation est disponible en ligne, ainsi que des sous-
titres (en français et en anglais) [sur meetings-archive.debian.net](https://meetings-archive.debian.net/pub/debian-meetings/2019/miniconf-marseille/2019-05-25/).

Cette rencontre a aussi été l'occasion pour chacun de rencontrer « en
vrai » les autres membres de l'équipe, pour certains, après plusieurs
années de travail collaboratif à distance. Ce genre de rencontre « in
real life » est précieux pour la construction de la cohésion d'équipe :
elle motive les gens en incarnant les personnes avec qui ils travaillent,
et facilite le dialogue écrit post rencontre.

À réitérer, dans le cadre d'une MiniDebConf ou pour un sprint dédié, car
cela pourra faire avancer des sujets communs clés.

Vous voulez contribuer à la traduction de Debian en Langue française ?
Contactez l'équipe de traduction sur la
[debian-l10n-french@lists.debian.org](mailto:debian-l10n-french@lists.debian.org)
ou rejoignez-nous sur le canal IRC [#debian-l10n-fr](https://webchat.oftc.net/?channels=#debian-l10n-fr)
sur le réseau IRC [OFTC](https://www.oftc.net/).




