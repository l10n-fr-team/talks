---
title: La localisation de Debian en langue française
author: L'équipe l10n-fr
date: 25 mai 2019
---

# Localisation ?

## Localisation !

+ fait de traduire du contenu dans une langue ou sa locale
+ abrégé _l10n_
+ concept proche de l’internationalisation (i18n)

# Utilité

## Pour les utilisateurs

+ Permettre l’accès à Debian dans leur langue à un maximum d’utilisateurs

> Nos priorités sont nos utilisateurs et les logiciels libres.

[Contrat social](https://www.debian.org/social_contract.fr.html)

## Pour soi

+ Apprendre de nouvelles choses
+ Découvrir tous les volets du projet
+ Découvrir les projets amont
+ Contribuer à son rythme
+ Improve your English…

# Périmètre

## Paquets

Paquets développés par/pour Debian :

+ dpkg
+ apt
+ aptitude
+ …

## Écrans Debconf

Écrans de configuration Debian, tels que l'écran d'installation d'Exim

## {data-background="images/exim-debconf-en.png"}

## {data-background="images/exim-debconf-fr.png"}

## Annonces

Tout ce qui paraît sur debian-news-french :

+ publications de Debian
+ DPN
+ annonces diverses

## Site web

Environ 10 000 pages (traduites à 89% !)

Toute la partie "statique" du site, mais aussi le contenu ajouté régulièrement :

+ publicity
+ annonces de sécurité (DSA, DLA)
+ programmes des candidats DPL
+ …

## Documentation

+ Logiciels spécifiques à Debian
+ Notes de publication
+ Référence du développeur Debian

## Descriptions de paquets

Description de **tous** les paquets (environ 59 000)

Interface web (DDTSS) ou courriel (DDTP)

## Ce qu'on ne traduit pas…

Logiciels traduits en amont

+ mutt
+ firefox
+ wget

## … Mais !

Dans certains cas la traduction en amont c'est nous !

Exemples :

+ unattended-upgrades
+ manpages
+ …

# Difficultés

##

+ Homogénéité entre les traductions
+ Traduction de jargons (bioinformatique, imagerie, maths)
+ Coordination entre :
    - le traducteur
    - le responsable du paquet
    - les développeurs en amont

# Suivi par courriels

## 

Le robot de la liste analyse les sujets de messages pour :

+ faire le suivi des traductions en cours
+ montrer les traductions à mettre à jour

Et pour nous humains :

+ suivre plus facilement dans sa messagerie
+ savoir où en sont nos traductions

(exemple)

# Contribuer

## Signaler

Signaler les erreurs ou traductions manquantes :

+ sur la liste debian-l10n-french
+ sur IRC

## Suivre les traductions

S’inscrire sur la liste [debian-l10n-french](https://lists.debian.org/debian-l10n-french/)

## Relire

Relire les propositions de traduction et proposer des correctifs :

+ sur la liste
+ sur le DDTSS

## Traduire

+ adopter des traductions abandonnées
+ entreprendre de nouvelles traductions (liste ou DDTSS)

# Pages de coordination

##

Plusieurs pages de suivi :

+ [annonces sur la liste](https://l10n.debian.org/coordination/french/fr.by_status.html)
+ [paquets gérés avec po](https://www.debian.org/international/l10n/po/fr)
+ [paquets gérés avec po4a](https://www.debian.org/international/l10n/po4a/fr)
+ [écrans debconf](https://www.debian.org/international/l10n/po-debconf/fr)
+ [site web](https://www.debian.org/devel/website/stats/fr)

## Liens utiles

+ [Projet de traduction Debian
(fr)](https://www.debian.org/international/french/)
+ [format des messages](https://www.debian.org/international/french/format)
+ [pseudo-urls](https://www.debian.org/international/l10n/pseudo-urls)
+ [MailingLists - Code de
conduite](https://www.debian.org/MailingLists/#codeofconduct)

# Nous contacter

##

+ Par courriel : [debian-l10n-french@lists.debian.org](mailto:debian-l10n-french@lists.debian.org)
+ Sur IRC : #debian-l10n-fr sur OFTC
+ Ici : venez nous voir !
